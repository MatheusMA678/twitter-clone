import React from "react";

const Header = () => {
  return (
    <div className="flex flex-col border-b border-primaryGray">
      <h1 className="font-bold text-xl p-4">Página Inicial</h1>
      <div className="flex items-center justify-around">
        <button className="py-4">
          <span className="after:w-full after:bg-primaryBlue after:h-1 after:rounded-lg relative after:absolute after:-bottom-4 after:left-0">
            Para você
          </span>
        </button>
        <button className="py-4">
          <span className="text-white/80">Seguindo</span>
        </button>
      </div>
    </div>
  );
};

export default Header;
