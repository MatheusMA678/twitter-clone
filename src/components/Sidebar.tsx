import Image from "next/image";
import React from "react";

import Logo from "@/assets/logo.svg";

const links = [
  { id: 1, label: "Página Inicial", href: "/" },
  { id: 2, label: "Explorar", href: "/explore" },
  { id: 3, label: "Notificações", href: "/notifications" },
  { id: 4, label: "Mensagens", href: "/messages" },
  { id: 5, label: "Itens salvos", href: "/saved" },
  { id: 6, label: "Twitter Blue", href: "/blue" },
  { id: 7, label: "Perfil", href: "/profile" },
  { id: 8, label: "Mais", href: "/more" },
];

const Sidebar = () => {
  return (
    <div className="flex flex-col gap-8 p-8">
      <Image src={Logo} alt="Logo do Twitter" width={32} />
      <nav className="flex flex-col gap-4">
        {links.map((link) => (
          <div
            className="cursor-pointer hover:text-white/80 transition"
            key={link.id}
          >
            {link.label}
          </div>
        ))}
      </nav>
    </div>
  );
};

export default Sidebar;
