import "./globals.css";

export const metadata = {
  title: "Twitter Clone",
  description: "Um clone do Twitter.",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="pt-br">
      <head>
        <link href="https://fonts.cdnfonts.com/css/chirp-2" rel="stylesheet" />
      </head>
      <body className="bg-black text-white">{children}</body>
    </html>
  );
}
