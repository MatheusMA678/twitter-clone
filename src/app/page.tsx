import Image from "next/image";
import { Inter } from "next/font/google";
import Sidebar from "@/components/Sidebar";
import Header from "@/components/Header";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <div
      className={`font-chirp h-screen grid grid-cols-[18rem_auto_20rem] bg-black text-white`}
    >
      <section className="">
        <Sidebar />
      </section>

      <section className="flex flex-col border-x border-primaryGray">
        <Header />
        <div className="flex-1"></div>
      </section>

      <section className=""></section>
    </div>
  );
}
