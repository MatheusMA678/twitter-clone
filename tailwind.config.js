/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        primaryGray: "#202327",
        primaryBlue: "#1A8CD8",
      },
      fontFamily: {
        chirp: "'Chirp', sans-serif",
      },
    },
  },
  plugins: [],
};
